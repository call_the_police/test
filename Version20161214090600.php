<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161214090600 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('CREATE TABLE public.project (project_id SERIAL NOT NULL, name VARCHAR(255) NOT NULL, create_date DATE NOT NULL DEFAULT CURRENT_DATE, update_date DATE NOT NULL DEFAULT CURRENT_DATE, PRIMARY KEY(project_id))');
        $this->addSql('CREATE TABLE public.operation (operation_id SERIAL NOT NULL, project_id INT NOT NULL, name VARCHAR(255) NOT NULL, create_date DATE NOT NULL DEFAULT CURRENT_DATE, update_date DATE NOT NULL DEFAULT CURRENT_DATE, PRIMARY KEY(operation_id))');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('DROP SEQUENCE public.project_project_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE public.operation_operation_id_seq CASCADE');
        $this->addSql('DROP TABLE public.project');
        $this->addSql('DROP TABLE public.operation');
    }
}
