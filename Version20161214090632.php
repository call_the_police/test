<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161214090632 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql('
          INSERT INTO "public"."project"("name") VALUES(\'project1\') RETURNING "project_id", "name", "create_date", "update_date";
        ');
        $this->addSql('
          INSERT INTO "public"."project"("name") VALUES(\'project2\') RETURNING "project_id", "name", "create_date", "update_date";
        ');
        $this->addSql('
            INSERT INTO "public"."operation"("project_id", "name") VALUES(1, \'operation1\') RETURNING "operation_id", "project_id", "name", "create_date", "update_date";
        ');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql('
            DELETE FROM "public"."operation" WHERE "operation_id"=1
        ');
        $this->addSql('
            DELETE FROM "public"."project" WHERE "project_id"=1;
        ');
        $this->addSql('
            DELETE FROM "public"."project" WHERE "project_id"=2;
        ');
    }
}
